[![pipeline status](https://gitlab.com/slcu/teamHJ/niklas/CostFunctions.jl/badges/master/pipeline.svg)](https://gitlab.com/slcu/teamHJ/niklas/CostFunctions.jl/commits/master)
[![coverage report](https://gitlab.com/slcu/teamHJ/niklas/CostFunctions.jl/badges/master/coverage.svg)](https://gitlab.com/slcu/teamHJ/niklas/CostFunctions.jl/commits/master)

# CostFunctions.jl

A set of efficient and well tested cost functions in Julia.

## Cost functions

### integral_cost
```julia
integral_cost(model::Function, x_data, y_data, max_stepsize)
```

returns the area of the model/data mismatch.

It does a linear interpolation between the data points and integrates the absolute difference between the model and this interpolation using a Riemann sum with a midpoint rule.

This works well with timecouse data if the timecourse is densely sampled in comparison to the shifting of features in the data.


### normalized_integral_cost
```julia
normalized_integral_cost(model::Function, x_data, y_data, max_stepsize)
```

same as above, but normalizes the value by the area of a linear interpolation of the data.

Beware that this blows up if the area under the data curve approaches zero.
